import yaml
import os

from fuseboxlabel.sheet import HagerVisionSheet


def main():
    import argparse

    parser = argparse.ArgumentParser(description="Fuxebox label generator")
    parser.add_argument("input", help="Input file", type=open)
    parser.add_argument("output", help="Output file", type=argparse.FileType('wb'))
    args = parser.parse_args()

    root_dir = os.path.dirname(os.path.abspath(__file__))
    with open(os.path.join(root_dir, 'i18n.yml')) as handle:
        i18n = yaml.load(handle)

    dataset = yaml.load(args.input)
    sheet = None
    if dataset['format'] == 'HagerVision':
        sheet = HagerVisionSheet()
    else:
        print("Format '{}' is not supported".format(dataset['format']))
        print("Supported formats:")
        print("* HagerVision")

    if not sheet:
        print("Unknown format '{}'".format(dataset['format']))
        exit(1)

    sheet.make_pdf(dataset, args.output, i18n)


if __name__ == '__main__':
    main()
