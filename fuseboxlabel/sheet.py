from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter, A4
from reportlab.lib.units import inch, cm


class Sheet:
    def __init__(self):
        self.i18n = None
        self.lang = 'en-uk'

    def make_pdf(self, dataset, filename, i18n):
        self.i18n = i18n
        if 'language' in dataset:
            if dataset['language'] not in i18n['rcd']:
                print("Language '{}' not implemented!".format(dataset['language']))
                exit(1)
            self.lang = dataset['language']

        c = canvas.Canvas(filename, pagesize=self.get_paper_info())
        c.setLineWidth(0.2)

        for page in self._chunks(dataset['rows'], self.rows_per_page()):
            i = 0
            self.page_init(c)
            for rowname in page:
                row = dataset['rows'][rowname]
                self.draw_row(c, i, row)
                i += 1
            self.page_finish(c)
            c.showPage()
        c.save()

    def _chunks(self, l, n):
        l = list(l)
        """Yield successive n-sized chunks from l."""
        for i in range(0, len(l), n):
            yield l[i:i + n]

    def get_paper_info(self):
        return A4

    def draw_row(self, c, index, rowdata):
        pass

    def page_init(self, c):
        pass

    def page_finish(self, c):
        pass

    def rows_per_page(self):
        return 5

    def set_fill_color(self, c, index):
        if index == 0:
            c.setFillColorRGB(114 / 255, 147 / 255, 203 / 255)
        elif index == 1:
            c.setFillColorRGB(225 / 255, 151 / 255, 76 / 255)
        elif index == 2:
            c.setFillColorRGB(132 / 255, 186 / 255, 91 / 255)
        elif index == 3:
            c.setFillColorRGB(211 / 255, 94 / 255, 96 / 255)


class HagerVisionSheet(Sheet):
    def get_paper_info(self):
        return A4

    def rows_per_page(self):
        return 3

    def draw_row(self, c, index, rowdata):
        unitwidth = 1.792 * cm
        colorheight = 0.7 * cm
        rowwidth = 3.6 * cm
        rowheight = unitwidth * 12

        offsetx = 0
        offsety = 4.23 * cm
        rowoffset = 1.5 * cm
        if index == 0:
            offsetx = rowoffset + (4 * rowwidth)
        elif index == 1:
            offsetx = rowoffset + (3 * rowwidth)
        elif index == 2:
            offsetx = rowoffset

        c.rect(offsetx, offsety, rowwidth, rowheight)

        unitoffset = rowheight
        for element in rowdata:
            width = element['width'] * unitwidth
            unitoffset -= width
            c.line(offsetx, offsety + unitoffset, offsetx + rowwidth, offsety + unitoffset)

            if 'color' in element:
                self.set_fill_color(c, element['color'])
                c.rect(offsetx + rowwidth - colorheight, unitoffset + offsety, colorheight, width, fill=1)

            texty = -1 * (unitoffset + offsety + width - 0.3 * cm)

            if 'code' in element:
                if 'color' in element:
                    c.setFillColorRGB(1, 1, 1)
                else:
                    c.setFillColorRGB(0, 0, 0)
                c.setFontSize(11)
                c.rotate(-90)
                textx = offsetx + rowwidth - colorheight + 0.2 * cm
                c.drawString(texty, textx, element['code'])
                c.rotate(90)

            if element['type'] == 'rcd':
                element['label'] = self.i18n['rcd'][self.lang]

            if 'label' in element:
                if isinstance(element['label'], list):
                    label = element['label']
                else:
                    label = [element['label']]

                if len(label) > 9:
                    if 'code' in element:
                        print("Too much labels in unit {}".format(element['code']))
                    else:
                        print("Too much labels in unit (without code)")
                    exit(1)

                labelmaxheight = rowwidth - colorheight
                textheight = 0.3 * cm
                labelblockheight = textheight * len(label)
                labeloffset = (labelmaxheight - labelblockheight) / 2
                c.rotate(-90)
                i = 0
                c.setFontSize(8)
                c.setFillColorRGB(0, 0, 0)
                for line in label:
                    textx = offsetx + rowwidth - colorheight - labeloffset - (textheight * i) - (0.3 * cm)
                    c.drawString(texty - 0.2 * cm, textx, line)
                    i += 1
                c.rotate(90)
