#!/usr/bin/env python3

from setuptools import setup, find_packages

from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='fuseboxlabel',
    version="0.1.0",
    description='A tool for creating printable labels for fuse boxes',
    long_description=long_description,
    long_description_content_type='text/markdown',
    author='Martijn Braam',
    author_email='martijn@brixit.nl',
    url='https://gitlab.com/MartijnBraam/fuseboxlabel',
    license='MIT',
    python_requires='>=3.4',
    classifiers=[
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'fuseboxlabel=fuseboxlabel:main',
        ],
    },
    include_package_data=True,
    install_requires=['reportlab']
)
